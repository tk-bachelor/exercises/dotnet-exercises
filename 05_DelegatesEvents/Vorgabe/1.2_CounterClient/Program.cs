﻿using System;
using _1._2_CounterLib;

namespace _1._2_CounterClient
{
    class Program
    {
        // TODO:
        // Event-Handler Methode "OnCountValueChanged"
        static void OnCountValueChanged(Counter c, CounterEventArgs args)
        {
            Console.WriteLine("Counter changed: value = {0}", args.Value);
        }

        [STAThread]
        static void Main(string[] args)
        {
            // TODO:
            // Counter auf den Initialwert "10" setzen
            Counter c = new Counter(10);

            // TODO:
            // CounterObserver-Instanzen "myObserver1" / "myObserver2" mit den Namen  "obs1" / "obs2"
            CounterObserver ob1 = new CounterObserver("obs1");
            CounterObserver ob2 = new CounterObserver("obs2");

            // TODO:
            // Registrieren der Event-Handler
            c.CountValueChanged += OnCountValueChanged;
            c.CountValueChanged += ob1.OnCountValueChanged;
            c.CountValueChanged += ob2.OnCountValueChanged;

            // TODO:
            // Inkrementieren des Counters
            c.Increment();

            // TODO:
            // Deregistrieren des Events von myObserver1
            c.CountValueChanged -= ob1.OnCountValueChanged;

            // TODO:
            // Counterwert auf "100" zurücksetzen.
            c.Reset(100);

            Console.ReadKey();
        }

        // TODO:
        // Implementieren Sie die Klasse "CounterObserver"
        class CounterObserver
        {
            private string name;
            public CounterObserver(string name)
            {
                this.name = name;
            }

            public void OnCountValueChanged(Counter c, CounterEventArgs args)
            {
                Console.WriteLine("Counter changed: obs: {0}, value = {1}", name, args.Value);
            }
        }
      

    }

}
