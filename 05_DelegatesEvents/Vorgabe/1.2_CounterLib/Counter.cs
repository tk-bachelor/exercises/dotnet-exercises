﻿using System;

namespace _1._2_CounterLib
{
    // TODO: Klasse CounterEventArgs implementieren
    public class CounterEventArgs : EventArgs
    {
        public int Value { get; private set; }
        public CounterEventArgs(int value)
        {
            this.Value = value;
        }
    }

    // TODO: Delegate CounterEventHandler
    public delegate void CounterEventHandler(Counter c, CounterEventArgs args);

    // TODO: Klasse Counter implementieren
    public class Counter
    {
        public event CounterEventHandler CountValueChanged;

        private int count;

        public int Count
        {
            get { return count; }
            set
            {
                this.count = value;
                CountValueChanged?.Invoke(this, new CounterEventArgs(Count));
            }
        }

        public Counter() : this(0)
        {
        }

        public Counter(int value)
        {
            this.Count = value;
        }

        public int Increment()
        {
            return ++Count;
        }

        public int Decrement()
        {

            return --Count;
        }

        public void Reset()
        {
            Count = 0;
        }

        public void Reset(int value)
        {
            this.Count = value;
        }
    }

}
