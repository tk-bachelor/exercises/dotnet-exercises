﻿using System;
using System.Collections;

namespace _1._3_ArrayListOperationen
{
    // TODO: Delegate "Action"
    // public delegate ...
    public delegate void Action(string i);

    // TODO: Delegate "Predicate"
    // public delegate ...
    public delegate Boolean Predicate(string s);

    class Program
    {
        static void Main()
        {
            ArrayList list = GetNameList();

            Console.WriteLine("--------------------------------------------------");
            Console.WriteLine("Liste aller Namen:");

            // TODO: list ausgeben (Methode ForAll)
            Action a = Print;
            ForAll(list, a);


            Console.WriteLine("--------------------------------------------------");
            Console.WriteLine("Liste aller Namen mit 'S':");

            // TODO: list auf Namen beginnend mit "S" filtern mit anonymer Methode (Methode Find)
            ArrayList listStartsWithS = Find(list, delegate (string s) { return s.StartsWith("S"); });

            // TODO: listStartsWithS ausgeben (Methode ForAll)
            ForAll(listStartsWithS, Console.WriteLine);


            Console.WriteLine("--------------------------------------------------");
            Console.WriteLine("Liste aller Namen mit Länge >= 15:");

            // TODO: list auf Namen mit Länge >= 15 filtern mit anonymer Methode (Methode Find)
            ArrayList listLength15Plus = Find(list, delegate (string s) { return s.Length >= 15; });

            // TODO: listLength15Plus ausgeben (Methode ForAll)
            ForAll(list, Console.WriteLine);


            Console.WriteLine("--------------------------------------------------");
            Console.WriteLine("Konkatenierter String:");

            string concatenated = String.Empty;

            // TODO: list in einen string konkatenieren (Methode ForAll)
            ForAll(list, delegate(string s) { concatenated += s; });

            // TODO: string "concatenated" auf Konsole ausgeben
            a(concatenated);
            Console.ReadKey();
        }

        // TODO: Methode ForAll(...) implementieren
        private static void ForAll(ArrayList list, Action action)
        {
            foreach (string s in list)
            {
                action(s);
            }
        }

        private static void Print(string s)
        {
            Console.WriteLine(s);
        }


        // TODO: Methode Find(...) implementieren
        private static ArrayList Find(ArrayList list, Predicate predicate)
        {
            ArrayList filteredList = new ArrayList();

            if (predicate == null) return filteredList;
            foreach (string s in list)
            {
                if (predicate(s))
                {
                    filteredList.Add(s);
                }
            }
            return filteredList;
        }

        private static ArrayList GetNameList()
        {
            return new ArrayList
            {
               "Shaun Byler",
               "Regenia Politte",
               "Earleen Huard",
               "Evelin Svoboda",
               "Phuong Briese",
               "Shea Sailor",
               "Kerri Seese",
               "Dulce Spalla",
               "Cedrick Bohr",
               "Pearline Abele",
               "Chas Grossi",
               "Teresa Winkelman",
               "Marilee Keaton",
               "Karisa Millender",
               "Nickie Borders",
               "Hoyt Eicher",
               "Ula Harjo",
               "Willodean Byerly",
               "Sirena Vives",
               "Johnnie Folkers"
            };
        }
    }
}
