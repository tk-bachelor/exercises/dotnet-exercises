﻿using System;

namespace _1._1_Konstruktor
{
    class Book
    {
        private string title;
        private string author;
        private bool available;

        public Book(string title, string author, bool available)
        {
            this.title = title;
            this.author = author;
            this.available = available;
        }

        // Konstruktor mit title / author
        public Book(string title, string author) : this(title, author, true)
        {

        }

        // TODO: Konstruktor mit title
        public Book(string title) : this(title, "anonymous", true)
        {

        }

        // TODO: Default-Konstruktor
        public Book() : this("untitled", "anonymous", true)
        {

        }

        // TODO: ToString()-Methode
        public override string ToString()
        {
            return string.Format("Book<{0},{1},{2}>", this.title, author, available);
        }

    }

}
