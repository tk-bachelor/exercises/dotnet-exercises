# .NET Technologies

## Week 1: 21.Sep 2016 - .NET Technologies Basics
- compile

## Week 2: 28.Sep 2016 - C# Basics
- String, Array, Statements etc.
- Recursive Programming

## Week 3: 05.Oct 2016 - Classes vs Structs
- Constructor
- Indexer
- Operator
- Structs
- Properties

## Week 4: 12.Oct 2016 - Inheritance & Garbage Collection
- Shape Inheritance
- Dynamic Binding (virtual, override and new)
- Interface

## Week 5: 19.Oct 2016 - Delegates
- Sort array with delegate comparators
- events
- Find with delegate predicates
- Interface vs Delegates

## Week 6: 25.Oct 2016 - Generics
- List<T> with IComparable
- .NET List<T>
- Type Constraints

## Week 7: 02.Nov 2016 - Iterators & Extensions
- Fibonacci
- City Collection
- Recursive List
- Simple Extension Methods for string
- Query Operators (HsrMultipleOf & HsrWhere)

## Week 8: 09.Nov 2016 - LINQ
- Extensions / Query Operators
- Extensions / Exception
- Query vs Lamda Expressions

## Week 9: 15.Nov 2016 - Entity Framework
- Code First Lab