﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace _1._2_CityCollection
{
	public class CityCollection : IEnumerable<string>
	{
		private string[] cities = { "Bern", "Basel", "Zürich", "Rapperswil", "Genf" };

        public IEnumerator<string> GetEnumerator()
        {
            foreach(string s in cities)
            {
                yield return s;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        
        public IEnumerable<string> Reverse
        {
            get
            {
                for(int i=cities.Length -1; i >= 0; i--)
                {
                    yield return cities[i];
                }
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
			CityCollection myColl = new CityCollection();

            // Ausgabe
            foreach (string s in myColl)
            {
                Console.WriteLine(s);
            }

            // Ausgabe in umgekehrter Reihenfolge 
            foreach (string s in myColl.Reverse)
            {
                Console.WriteLine(s);
            }

            Console.ReadKey();
        }
    }
}
