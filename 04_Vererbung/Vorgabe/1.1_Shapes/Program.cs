﻿using System;

namespace _1._1_Shapes
{
    class Program
    {
        static void Main(string[] args)
        {
            // TODO: Erzeuge Square-Objekt
            Square sq = new Square(1.0f, 2.0f, 4.0f);

            // TODO: Ausgabe Degrees
            Console.WriteLine(sq.Degrees);

            // TODO: Ausgabe Area()
            Console.WriteLine(sq.Area());

            // TODO: Ausgabe X1, Y1, X2, Y2
            Console.WriteLine("X1:{0}, X2: {1}, Y1: {2}, Y2: {3}", sq.X1, sq.X2, sq.Y1, sq.Y2);
            Console.ReadKey();
        }
    }
}
