using System;

namespace MultipleAssemblies
{
    public class HelloConsole
    {
        public static void Speak()
        {
            Console.WriteLine("Hello from Console / HelloConsole.Speak()");
        }
    }
}