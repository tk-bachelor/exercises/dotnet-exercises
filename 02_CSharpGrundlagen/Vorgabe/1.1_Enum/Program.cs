﻿using System;

namespace _1._1_Enum
{
    // TODO: enum "Volume" implementieren
    enum Volume { Unknown, Low, Medium, High };

    class Program
    {
       
        static void Main(string[] args)
        {
            // TODO: Volume-Variable erstellen
            Volume v = Volume.Low;

            // TODO: Ausgabe über PrintVolume
            PrintVolume(v);            

            // TODO: volumeString via ParseVolume(...) in Volume-Variable speichern
            string volumeString = "High";
            Volume high = ParseVolume(volumeString);
            // TODO: Ausgabe über PrintVolume
            PrintVolume(high);
            Console.ReadKey();
        }

        // TODO: public static void PrintVolume... 
        public static void PrintVolume(Volume v)
        {
            Console.WriteLine(string.Format("The volume is {0} / value '{1}'", v.ToString(), (int)v));
        }
        // Ausgabe: The volume is 'Low' / value '2'

        // TODO: public static Volume ParseVolume...
        public static Volume ParseVolume(string text)
        {
            return (Volume)Enum.Parse(typeof(Volume), text);
        }
    }
}
