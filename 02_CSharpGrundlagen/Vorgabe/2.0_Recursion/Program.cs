﻿using System;

namespace _2._0_Recursion
{
    class Program
    {
        static int[] damen = new int[8];
        static bool[] zeileFrei = { true, true, true, true, true, true, true, true };
        static bool[] diagLuRoFrei = { true, true, true, true, true, true, true, true, true, true, true, true, true, true, true };
        static bool[] diagLoRuFrei = { true, true, true, true, true, true, true, true, true, true, true, true, true, true, true };
        static int anzahlLsg;

        static void SetzeDame(int spalte, int zeile)
        {
            damen[spalte] = zeile;
            zeileFrei[zeile] = false;
            diagLuRoFrei[spalte + zeile] = false;
            diagLoRuFrei[spalte - zeile + 7] = false;
        }

        static void EntferneDame(int spalte, int zeile)
        {
            zeileFrei[zeile] = true;
            diagLuRoFrei[spalte + zeile] = true;
            diagLoRuFrei[spalte - zeile + 7] = true;
        }

        static bool TestPosition(int spalte, int zeile)
        {
            return
                zeileFrei[zeile] &&
                diagLuRoFrei[zeile + spalte] &&
                diagLoRuFrei[spalte - zeile + 7];
        }

        static void PrintLsg()
        {
            Console.WriteLine();
            foreach (var zeile in damen)
            {
                Console.Write(" {0} ", zeile + 1);
            }
            anzahlLsg++;
        }

        // TODO: Implementiere Rekursion
        static void Versuche(int spalte)
        {
            for (int zeile = 0; zeile < 8; zeile++)
            {
                if (TestPosition(spalte, zeile))
                {
                    SetzeDame(spalte, zeile);
                    if (spalte < 7)
                    {
                        //Rekursion
                        Versuche(spalte + 1);
                    }
                    else
                    {
                        PrintLsg();
                    }
                    //Backtracking
                    EntferneDame(spalte, zeile);
                }
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("x1 x2 x3 x4 x5 x6 x7 x8");
            Console.WriteLine("-----------------------");

            // TODO: Rekursion starten
            Versuche(0);

            // TODO: Anzahl der gefundenen Lösungen ausgeben
            Versuche(0);
            Console.WriteLine("\nAnzahl Lösungen {0}", anzahlLsg);


            Console.ReadKey();
        }
    }
}
